--
-- Loading plugins and package manager.
require("plugins")
require("os-detector")
--
-- Basic plugin start-ups.
require("ibl").setup()
--
-- Custom plugin start-ups.
require("cmp-config")
require("lualine-config")
require("telescope-config")
require("neo-tree-config")
require("mason-config")
require("formatter-config")
require("tabs")
require("themery-config")
require("tokyonight-config.transparent")
--
-- Skin.
require("theme")
--
-- NVim configuration.
-- * Personalized configuration:
vim.o.exrc = true
-- * Line position:
vim.cmd("set number")
vim.cmd("set cursorline")
-- * Indentation:
vim.cmd("set autoindent")
vim.cmd("set tabstop=4")
vim.cmd("set shiftwidth=4")
vim.cmd("set smarttab")
vim.cmd("set softtabstop=4")
vim.cmd("set expandtab")
-- * Mouse:
vim.cmd("set mouse=a")
-- * Others:
vim.cmd("set nowrap")
vim.cmd("set encoding=utf-8")
-- vim.cmd("set foldmethod=indent")
-- * Shortcuts:
vim.keymap.set("n", "<C-e>", ":Neotree float reveal_force_cwd<CR>")
-- vim.keymap.set("n", "<C-t>", ":tabnew<CR>:term<CR>")
vim.keymap.set("n", "<C-o>f", ":Telescope find_files<CR>")
vim.keymap.set("n", "<C-o>d", ":Telescope lsp_dynamic_workspace_symbols<CR>")
vim.keymap.set("n", "<C-o>r", ":Telescope lsp_references<CR>")
vim.keymap.set("n", "<C-o>g", ":Telescope live_grep<CR>")
vim.keymap.set("n", "<C-t>", ":Telescope scope buffers<CR>")
--
-- Operating System customizations
-- if os.name() == "Linux" then
-- vim.cmd("highlight Normal ctermbg=none")
-- vim.cmd("highlight NonText ctermbg=none")
-- end
