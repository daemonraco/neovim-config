require("neo-tree").setup({
	filesystem = {
		filtered_items = {
			visible = true,
			show_hidden_count = false,
			hide_dotfiles = false,
			hide_hidden = false,
			hide_gitignored = false,
		},
	},
})
