require("utils")

local configFile = os.getenv("HOME") .. "/.config/nvim/lua/theme.lua"

if not FileExists(configFile) then
	local f = io.open(configFile, "w")
	f:write("-- Themery block\n")
	f:write("-- end themery block\n")
	f:close()
end

require("themery").setup({
	themes = vim.fn.getcompletion("", "color"),
	themeConfigFile = configFile,
	livePreview = true,
})
