require("tokyonight").setup({
    transparent = true,
    styles = {
        floats = "transparent",
        sidebars = "transparent",
    },
})
