local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

-- Install your plugins here
return packer.startup(function(use)
	use("wbthomason/packer.nvim") -- Have packer manage itself.

	-- Theme
	use("zaldih/themery.nvim")
	use("catppuccin/nvim")
	use("folke/tokyonight.nvim")

	-- LSP and Language support
	use("folke/neodev.nvim")
	use("williamboman/mason.nvim") -- LSP and Language support
	use("williamboman/mason-lspconfig.nvim")
	use({
		"neovim/nvim-lspconfig",
		dependencies = {
			"folke/neodev.nvim",
			"williamboman/mason.nvim",
		},
	})
	use("mfussenegger/nvim-lint")
	use("stevearc/conform.nvim")

	use("nvim-lualine/lualine.nvim") -- Fancy status bar

	use("mg979/vim-visual-multi") -- Column editor (@check)

	use("nvim-treesitter/nvim-treesitter")

	use("nvim-tree/nvim-web-devicons")

	-- Fuzzy search
	use({
		"nvim-telescope/telescope.nvim",
		requires = {
			"nvim-lua/plenary.nvim",
			"BurntSushi/ripgrep",
		},
	})

	-- Autocomplete (like COC but with no NodeJS required)
	use("hrsh7th/cmp-nvim-lsp")
	use("hrsh7th/cmp-buffer")
	use("hrsh7th/cmp-path")
	use("hrsh7th/cmp-cmdline")
	use("hrsh7th/nvim-cmp")
	use("L3MON4D3/LuaSnip")
	use("saadparwaiz1/cmp_luasnip")
	use("rafamadriz/friendly-snippets")

	-- File explorer.
	use({
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v3.x",
		requires = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons",
			"MunifTanjim/nui.nvim",
		},
	})

	-- Comments toggle (shortcut gcc)
	use("tpope/vim-commentary")

	-- Nice tabs.
	use({
		"tiagovla/scope.nvim",
		requires = {
			"nvim-telescope/telescope.nvim",
		},
	})
	use({
		"akinsho/bufferline.nvim",
		tags = "*",
		requires = {
			"nvim-tree/nvim-web-devicons",
		},
	})

	-- Vertical rulers.
	use("lukas-reineke/indent-blankline.nvim")

	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
