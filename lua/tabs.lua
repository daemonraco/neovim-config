require("telescope").load_extension("scope")
require("scope").setup({})

vim.opt.termguicolors = true
require("bufferline").setup({
	options = {
		mode = "tabs",
	},
})
